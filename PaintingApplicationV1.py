
from PyQt5.QtWidgets import QApplication, QMainWindow,QAction, QFileDialog, QWidget, QLabel, QVBoxLayout, QColorDialog, QSlider
from PyQt5.QtGui import QIcon, QImage, QPainter, QPen, QPixmap, QBrush
import sys
from PyQt5.QtCore import Qt, QPoint

class PaintingApplication(QMainWindow): # documentation https://doc.qt.io/qt-5/qmainwindow.html
    '''
    Painting Application class
    '''

    def __init__(self):
        super().__init__()

        # set window title
        self.setWindowTitle("Paint Application")

        # set the windows dimensions
        top = 400
        left = 400
        width = 800
        height = 600
        self.setGeometry(top, left, width, height)

        #set the icon
        # windows version
        self.setWindowIcon(QIcon("./icons/paint-brush.png")) # documentation: https://doc.qt.io/qt-5/qwidget.html#windowIcon-prop
        # mac version - not yet working
        # self.setWindowIcon(QIcon(QPixmap("./icons/paint-brush.png")))

        # image settings (default)
        self.image = QImage(self.size(), QImage.Format_RGB32) # documentation: https://doc.qt.io/qt-5/qimage.html#QImage-1
        self.image.fill(Qt.white) # documentation: https://doc.qt.io/qt-5/qimage.html#fill-1

        # draw settings (default)
        self.drawing = False
        self.brushSize = 3
        self.brushColor = Qt.black # documenation: https://doc.qt.io/qtforpython/PySide2/QtCore/Qt.html
        self.penStyle = Qt.SolidLine #set penStyle to solid line by default
        self.joinStyle = Qt.RoundJoin
        self.capStyle = Qt.RoundCap
        # reference to last point recorded by mouse
        self.lastPoint = QPoint() # documenation: https://doc.qt.io/qt-5/qpoint.html
        self.helpWindow = HelpWindow()
        # set up menus
        mainMenu = self.menuBar() # create and a menu bar
        fileMenu = mainMenu.addMenu(" File") # add the file menu to the menu bar, the space is required as "File" is reserved in Mac
        brushSizeMenu = self.addToolBar(" Brush Size") # add the "Brush Size" menu to the menu bar
        brushColorMenu = self.addToolBar(" Brush Colour") # add the "Brush Colour" menu to the menu bar
        # create a slider for selection of the size
        self.sizeSlider = QSlider(Qt.Horizontal, self)
        self.sizeSlider.setMinimum(1)
        self.sizeSlider.setMaximum(50)
        self.sliderLabel = QLabel('3', self) # create a label to display size. 3 by default as default size is 3

        self.sizeSlider.valueChanged.connect(self.changeBrushSize)
        brushSizeMenu.addWidget(self.sizeSlider)
        brushSizeMenu.addWidget(self.sliderLabel)
        penStyleMenu = self.addToolBar(" Pen Style")# add the "Pen Style" menu to the menu bar
        joinStyleMenu = self.addToolBar(" Join Style")# add the "Join Style" menu to the menu bar
        capStyleMenu = self.addToolBar(" Cap Style")# add the "Cap Style" menu to the menu bar
        # save menu item
        saveAction = QAction(QIcon("./icons/save.png"), "Save", self)   # create a save action with a png as an icon, documenation: https://doc.qt.io/qt-5/qaction.html
        saveAction.setShortcut("Ctrl+S")                                # connect this save action to a keyboard shortcut, documentation: https://doc.qt.io/qt-5/qaction.html#shortcut-prop
        fileMenu.addAction(saveAction)                                  # add the save action to the file menu, documentation: https://doc.qt.io/qt-5/qwidget.html#addAction
        saveAction.triggered.connect(self.save)                         # when the menu option is selected or the shortcut is used the save slot is triggered, documenation: https://doc.qt.io/qt-5/qaction.html#triggered

        # clear
        clearAction = QAction(QIcon("./icons/clear.png"), "Clear", self) # create a clear action with a png as an icon
        clearAction.setShortcut("Ctrl+C")                                # connect this clear action to a keyboard shortcut
        fileMenu.addAction(clearAction)                                  # add this action to the file menu
        clearAction.triggered.connect(self.clear)                        # when the menu option is selected or the shortcut is used the clear slot is triggered

        #open
        openAction = QAction(QIcon("./icons/open.png"), " Open", self)
        openAction.setShortcut("Ctrl+O")
        fileMenu.addAction(openAction)
        openAction.triggered.connect(self.open)

        # help
        helpAction = QAction(QIcon("./icons/help.png"), "Help", self)
        helpAction.setShortcut("Ctrl+H")
        fileMenu.addAction(helpAction)
        helpAction.triggered.connect(self.help)

        # exit
        exitAction =QAction(QIcon("./icons/exit.png"), "Exit", self)
        exitAction.setShortcut("Ctrl+E")
        fileMenu.addAction(exitAction)
        exitAction.triggered.connect(self.exit)
        # brush colors
        blackAction = QAction(QIcon("./icons/black.png"), "Black", self)
        blackAction.setShortcut("Ctrl+B")
        brushColorMenu.addAction(blackAction);
        blackAction.triggered.connect(self.black)

        redAction = QAction(QIcon("./icons/red.png"), "Red", self)
        redAction.setShortcut("Ctrl+R")
        brushColorMenu.addAction(redAction);
        redAction.triggered.connect(self.red)

        greenAction = QAction(QIcon("./icons/green.png"), "Green", self)
        greenAction.setShortcut("Ctrl+G")
        brushColorMenu.addAction(greenAction);
        greenAction.triggered.connect(self.green)

        yellowAction = QAction(QIcon("./icons/yellow.png"), "Yellow", self)
        yellowAction.setShortcut("Ctrl+Y")
        brushColorMenu.addAction(yellowAction)
        yellowAction.triggered.connect(self.yellow)
        # add customized color option
        customizeAction = QAction(QIcon("./icons/customize.png"),"Customize", self)
        brushColorMenu.addAction(customizeAction)
        customizeAction.triggered.connect(self.customize)
        # add pen styles
        solidLineAction = QAction(QIcon("./icons/solidline.png") ,"Solid Line", self)
        penStyleMenu.addAction(solidLineAction)
        solidLineAction.triggered.connect(self.solidLine)

        dashLineAction = QAction(QIcon("./icons/dashline.png") ,"Dash Line", self)
        penStyleMenu.addAction(dashLineAction)
        dashLineAction.triggered.connect(self.dashLine)

        dotLineAction = QAction(QIcon("./icons/dotline.png"),"Dot Line", self)
        penStyleMenu.addAction(dotLineAction)
        dotLineAction.triggered.connect(self.dotLine)

        dashDotLineAction = QAction(QIcon("./icons/dashdot.png"),"Dash Dot Line", self)
        penStyleMenu.addAction(dashDotLineAction)
        dashDotLineAction.triggered.connect(self.dashDotLine)

        # add join styles
        bevelJoinAction = QAction(QIcon("./icons/bevel-join.png")," Bevel Join", self)
        joinStyleMenu.addAction(bevelJoinAction)
        bevelJoinAction.triggered.connect(self.bevelJoin)

        roundJoinAction = QAction(QIcon("./icons/round-join.png")," Round Join", self)
        joinStyleMenu.addAction(roundJoinAction)
        roundJoinAction.triggered.connect(self.roundJoin)

        miterJoinAction = QAction(QIcon("./icons/miter-join.png")," Miter Join", self)
        joinStyleMenu.addAction(miterJoinAction)
        miterJoinAction.triggered.connect(self.miterJoin)
        # add cap options
        roundCapAction = QAction(QIcon("./icons/roundcap.png")," Round Cap", self)
        capStyleMenu.addAction(roundCapAction)
        roundCapAction.triggered.connect(self.roundCap)

        flatCapAction = QAction(QIcon("./icons/flatcap.png")," Flat Cap", self)
        capStyleMenu.addAction(flatCapAction)
        flatCapAction.triggered.connect(self.flatCap)

        squareCapAction = QAction(QIcon("./icons/squarecap.png")," Square Cap", self)
        capStyleMenu.addAction(squareCapAction)
        squareCapAction.triggered.connect(self.squareCap)

    # event handlers
    def mousePressEvent(self, event):       # when the mouse is pressed
        if event.button() ==Qt.LeftButton:  # if the pressed button is the left button
            self.drawing = True             # enter drawing mode
            self.lastPoint = event.pos()    # save the location of the mouse press as the lastPoint
            print(self.lastPoint)           # print the lastPoint for debigging purposes

    def mouseMoveEvent(self, event):                        # when the mouse is moved, documenation
     if event.buttons() & Qt.LeftButton & self.drawing:     # if there was a press, and it was the left button and we are in drawing mode
            painter = QPainter(self.image)                  # object which allows drawing to take place on an image
            pen = QPen(self.brushColor, self.brushSize, self.penStyle, self.capStyle, self.joinStyle)
            painter.setPen(pen)
            # allows the selection of brush colour, brish size, line type, cap type, join type

            painter.drawLine(self.lastPoint, event.pos())   # draw a line from the point of the orginal press to the point to where the mouse was dragged to
            self.lastPoint= event.pos()                     # set the last point to refer to the point we have just moved to, this helps when drawing the next line segment
            self.update()                                   # call the update method of the widget which calls the paintEvent of this class

    def mouseReleaseEvent(self, event):                     # when the mouse is released
        if event.button == Qt.LeftButton:                   # if the released button is the left button
            self.drawing = False                            # exit drawing mode

    # paint events
    def paintEvent(self, event):
        # you should only create and use the QPainter object in this method, it should be a local variable
        canvasPainter = QPainter(self)                      # create a new QPainter object
        canvasPainter.drawImage(self.rect(), self.image, self.image.rect()) # draw the image

    # resize event - this fuction is called
    def resizeEvent(self, event):
        self.image = self.image.scaled(self.width(), self.height())

    # slots
    def save(self):
        filePath, _ = QFileDialog.getSaveFileName(self, "Save Image","", "PNG(*.png);;JPG(*.jpg *.jpeg);;All Files (*.*)")
        if filePath =="": # if the file path is empty
            return # do nothing and return
        self.image.save(filePath) # save file image to the file path
    def help(self, checked):
        self.helpWindow.show()
    def exit(self):
        sys.exit()
    def clear(self):
        self.image.fill(Qt.white)   # fill the image with white
        self.update()               # call the update method of the widget which calls the paintEvent of this class
    def threepx(self):              # the brush size is set to 3
        self.brushSize = 3

    def fivepx(self):
        self.brushSize = 5

    def sevenpx(self):
        self.brushSize = 7

    def ninepx(self):
        self.brushSize = 9

    def black(self):                # the brush color is set to black
        self.brushColor = Qt.black

    def black(self):
        self.brushColor = Qt.black

    def red(self):
        self.brushColor = Qt.red

    def green(self):
        self.brushColor = Qt.green

    def yellow(self):
        self.brushColor = Qt.yellow
    def customize(self):
        # use QColorDialog widget to select the color
        self.brushColor = QColorDialog.getColor()
    def solidLine(self):
        self.penStyle = Qt.SolidLine
    def dashLine(self):
        self.penStyle = Qt.DashLine
    def dotLine(self):
        self.penStyle = Qt.DotLine
    def dashDotLine(self):
        self.penStyle = Qt.DashDotLine
    def customDashLine(self):
        self.penStyle = Qt.CustomDashLine
    def bevelJoin(self):
        self.joinStyle = Qt.BevelJoin
    def roundJoin(self):
        self.joinStyle = Qt.RoundJoin
    def miterJoin(self):
        self.joinStyle = Qt.MiterJoin
    def roundCap(self):
        self.capStyle = Qt.RoundCap
    def flatCap(self):
        self.capStyle = Qt.FlatCap
    def squareCap(self):
        self.capStyle = Qt.SquareCap
    def changeBrushSize(self):
        self.brushSize = self.sizeSlider.value()
        self.sliderLabel.setText(str(self.brushSize))
    # open a file
    def open(self):

        filePath, _ = QFileDialog.getOpenFileName(self, "Open Image", "",
                                                  "PNG(*.png);;JPG(*.jpg *.jpeg);;All Files (*.*)")
        if filePath == "":   # if not file is selected exit
            return
        with open(filePath, 'rb') as f: #open the file in binary mode for reading
            content = f.read() # read the file
        self.image.loadFromData(content) # load the data into the file
        width = self.width() # get the width of the current QImage in your application
        height = self.height() # get the height of the current QImage in your application
        self.image = self.image.scaled(width, height) # scale the image from file and put it in your QImage
        self.update() # call the update method of the widget which calls the paintEvent of this class

class HelpWindow(QWidget):
    def __init__(self):
        super().__init__()
        layout = QVBoxLayout()
        self.setGeometry(50, 50, 50, 50)
        self.label = QLabel("Welcome to Paint Application. In this application you can open JPG and PNG files, edit them and then save them"
                            "\nYou can select the color, brush thickness, join style and cap style. ")
        layout.addWidget(self.label)
        self.setWindowTitle("Help")
        self.setLayout(layout)


if __name__=="__main__":
    app = QApplication(sys.argv)
    window = PaintingApplication()
    window.show()
    app.exec() # start the event loop running